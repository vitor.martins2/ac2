from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello World! Isto é uma aplicação flask"

@app.route('/hello2')
def hello_world2():
    return "Hello World2! Isto é uma aplicação flask"

@app.route('/hello3')
def hello_world3():
    return "Hello World3! Isto é uma aplicação flask"

@app.route('/hello4')
def hello_world4():
    return "Hello World4! Isto é uma aplicação flask"

@app.route('/hello5')
def hello_world5():
    return "Hello World5! Isto é uma aplicação flask"

if __name__ == '__main__':
    app.run(debug=True)
