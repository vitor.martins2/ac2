import unittest
from app import *


class TestFlaskApp(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Hello World! Isto é uma aplicação flask")

    def test_hello_world2(self):
        response = self.app.get('/hello2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Hello World2! Isto é uma aplicação flask")

    def test_hello_world3(self):
        response = self.app.get('/hello3')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Hello World3! Isto é uma aplicação flask")

    def test_hello_world4(self):
        response = self.app.get('/hello4')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Hello World4! Isto é uma aplicação flask")

    def test_hello_world5(self):
        response = self.app.get('/hello5')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Hello World5! Isto é uma aplicação flask")

if __name__ == '__main__':
    unittest.main()
