## AC2 - Flask

#### Descrição
Este projeto é um exemplo simples de uma aplicação Flask que exibe "Hello, World!" e é executada dentro de um contêiner Docker. Ele foi criado como parte de um projeto acadêmico da faculdade para demonstrar a criação de uma aplicação web em Python e sua implantação em um ambiente de contêinerizado.

#### Funcionalidades
- Exibe a mensagem "Hello, World!" em uma página web.
- Containerizado com Docker para facilitar a implantação.

#### Pré-requisitos
Antes de iniciar, certifique-se de ter o seguinte instalado:
- Docker - Para criar e executar contêineres.

#### Instalação
1. Clone este repositório
```
git clone https://gitlab.com/my-gitlab-group6315134/ac2.git
```
2. Navegue até o projeto
```
cd ac2
```
3. Crie a imagem Docker
```
docker build --tag my-app-flask/docker .
```
4. Execute o contêiner
```
docker run -d -p 5000:5000 my-app-flask/docker
```
5. Acesse a aplicação em seu navegador em http://localhost:5000.

#### Uso
Este projeto é um exemplo simples de Flask que pode ser usado como ponto de partida para construir aplicativos mais complexos. Personalize o código-fonte em app.py para atender às suas necessidades.

#### Alunos:
- Vitor de Camargo Martins RA 2203267
- Beatriz Fernandes Martins RA 2203316
- Fabiano Quirino Silva RA 2203446
- Gabriel Ramalho de Brito RA 2203321
- Guilherme do Espirito Santo Moreira RA 2203381
- Luigi Carlo Bellotto RA 2203247
- Yuri Rodrigues Fideles RA 2203189
